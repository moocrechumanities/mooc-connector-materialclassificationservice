
#intall fleep library
#install flask
from flask import Flask
from flask_restful import Api, Resource

fleep_course_edx = Flask(__name__)
api = Api(fleep_course_edx)

import logging
from fileFormatDetermination import Info,get,supported_types,supported_extensions,supported_mimes
#import fleep
import os

from cal_time_pdf import cal_time
from cal_time_quiz import cal_time_quiz
from cal_time_html import calTimeHtml

#/home/ec2-user/Document/terrorism/
#/home/zuhair/Desktop/MoocRec/coursera-dl-master/terrorism/
CouseraUrl = '/home/ec2-user/Document/course/terrorism/'

mat_percentage = {}
total_percentage = {}
learning_materials = []
learning_materials_per = []
pdf_per = {}
quiz_per = {}
video = {}
pdf = {}
quiz = {}
dic_learning_materials = {}


def classifyCousera(url):
    for dirpath, dirnames, files in os.walk(url):
#        logging.info('Found Directory:  {}'.format(dirpath))
        print(f'Found directory: {dirpath}')
        for file_name in files:
            with open(dirpath+'/'+file_name , "rb") as file:
                info = get(file.read(128))
                if len(info.type) == 0:
    # print(file_name + "        "+ "quiz")
                    timeQuiz = cal_time_quiz.calTimeOfQuiz(dirpath+'/'+file_name) 
                    quiz.update({'name':file_name,'con_type':'quiz','time':timeQuiz})                                       
                    learning_materials.append(quiz.copy())
                    quiz.clear
                elif len(info.type) > 1:
    #               print(file_name + "        "+ info.type[1])#this is for pdf
                    timePdf =  cal_time.calTimeOfPdf(dirpath+'/'+file_name)
                    pdf.update({'name':file_name,'con_type':'pdf','time':timePdf})
                    learning_materials.append(pdf.copy())
                    pdf.clear
                else:
    #                    print(file_name + "        "+ info.type[0])#this is for video
                    video.update({file_name:file_name})
                        
#    print(learning_materials)#this is the learning materials with time in list           



def calPercentage():
    total_pdf_time = 0
    total_quiz_time = 0
    total_html_time = 0
    total_video_time =0
    
    for item in range(len(learning_materials)):
        if learning_materials[item]['con_type'] == 'pdf':
            total_pdf_time += learning_materials[item]['time']
        elif learning_materials[item]['con_type']== 'quiz':
            total_quiz_time += learning_materials[item]['time']
        else:
            total_video_time += learning_materials[item]['time']
    
    pdf_weight = (total_pdf_time/(total_pdf_time + total_quiz_time))*100
    pdf_weight = round(pdf_weight)
    quiz_weight = (total_quiz_time/(total_pdf_time + total_quiz_time))*100
    quiz_weight = round(quiz_weight)
    
    quiz_per.update({'con_type':'quiz','percentage':quiz_weight})
    pdf_per.update({'con_type':'pdf','percentage':pdf_weight})
    
    total_percentage.update({'pdf':pdf_per,'quiz':quiz_per})
    
#    mat_percentage.update({'pdf':pdf_per,'quiz':quiz_per})
    mat_percentage.update({"attribute":"weight","percentage":total_percentage})
    
    learning_materials_per.append(mat_percentage.copy())
#    learning_materials_per.append(quiz_per.copy())
#    learning_materials_per.append(pdf_per.copy())
    
    return learning_materials_per


class Home(Resource):
    def get(self):
        return "Api Called",200
#This is api call for percentage of a course
#    /material_weight/weight
class Material_Weight(Resource):    
    def get(self,weight):
        classifyCousera(CouseraUrl)
        list_for_per = calPercentage()
        for dic_weight in list_for_per:
            if(weight == dic_weight["attribute"]):
                return dic_weight["percentage"], 200
        return "Weight not found", 404



#This is api call for get learning Materials acordingg to the content type
#   /learningmaterials/pdf     or    /learningmaterials/quiz
class LearningMaterials(Resource):
    def get(self,mat_type):
        classifyCousera(CouseraUrl)
        dic_learning_materials.clear()
        for dic_materials in learning_materials:
            if(mat_type == dic_materials['con_type']):
                dic_learning_materials.update({dic_materials['name']:dic_materials})
#                return dic_pdf, 200
#        return "pdf not found", 404
        if len(dic_learning_materials) == 0:
            return "Materials not found", 404
        return dic_learning_materials, 200
        
        
        

api.add_resource(Home,"/home")       
api.add_resource(Material_Weight, "/material_weight/<string:weight>")
api.add_resource(LearningMaterials, "/material/<string:mat_type>")
fleep_course_edx.run(host='0.0.0.0',port=3002,debug=True)

#def main():
#    classifyEdX(EdXUrl)
#    list_for_per = calPercentage()
#    print(learning_materials)
#    for dic_materials in learning_materials:
#        if dic_materials['con_type'] == 'pdf':
#            li_learning_materials.append(dic_materials)
#    print(li_learning_materials)
#    
#if __name__ == '__main__':
#    main()

#import magic
#magic.from_file("Book1.csv")


