import os
import sys
import re
import codecs
import nltk
#import readtime
nltk.download('stopwords')
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
from getReadTime import of_text,of_html,of_markdown

class cal_time_quiz():
    #Cleaning the data with keras nltk
    def cleanData(html):

        data = re.sub('[^a-zA-Z]',' ',html) #keep only all small and capital letters. if there is anything else replace with ' '
        data = data.lower()
        data = data.split()
        ps = PorterStemmer() #loved change to common word love
        data = [ps.stem(word) for word in data if not word in set(stopwords.words('english'))]
        data = ' '.join(data)
        return data

    #Calculate the speed of the text in the file
    def calculateSpeed(html):
        speed = of_html(html)
        return speed

    def calTimeOfQuiz(quizFile):
        htmlFile = quizFile
        try:
            if os.path.exists(htmlFile):
                print("\nQuiz file found .........................!\n")
        except OSError as err:
            print(err.reason)
            exit(1)
    
        print("Loading quiz content .........................!\n")
        f=codecs.open(htmlFile, 'r')
        html_content = f.read()
        print ("Quiz content loaded .........................!\n")
        
                    #calculate the reading time
        print("Started : html file time calculate .........................!\n")
        reading_time = cal_time_quiz.calculateSpeed(html_content)
        seconds = reading_time.seconds
        minutes, seconds = divmod(seconds, 60)
        
        html_content = html_content.split("<h3>")
        html_content.pop(0)
        quiz_questions = len(html_content)
        time_for_quiz = (minutes+1)+(quiz_questions*2)
        print("Finished : html file time calculated .........................!\n")
        
        return time_for_quiz
        

#def main():
#    if len(sys.argv)!=2:
#        print('command usage: python know_count.py FileName')
#        exit(1)
#    else:
#        htmlFile = sys.argv[1]
#
#        # check if the specified file exists or not
#        try:
#            if os.path.exists(htmlFile):
#                print("\nfile found!\n")
#        except OSError as err:
#            print(err.reason)
#            exit(1)
#
#        print("Loading quiz content...\n")
#        f=codecs.open(htmlFile, 'r')
#        html_content = f.read()
#        print ("quiz content loaded\n")
#
#        #calculate the reading time
#        reading_time =calculateSpeed(html_content)
#        seconds = reading_time.seconds
#        minutes, seconds = divmod(seconds, 60)
#
#        html_content = html_content.split("<P>")
#        html_content.pop(0)
#        quiz_questions = len(html_content)
#        time_for_quiz = (minutes+1)+(quiz_questions*2)
#        print(time_for_quiz)
#
#
#if __name__ == '__main__':
#    main()



