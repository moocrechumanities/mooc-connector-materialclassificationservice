import os
#import sys
import re
import PyPDF2
import nltk
#import readtime
nltk.download('stopwords')
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
from getReadTime import of_text,of_html,of_markdown

class cal_time():
    #Get the page count 
    def getPageCount(pdf_file):
    
        pdfFileObj = open(pdf_file, 'rb')
        pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
        pages = pdfReader.numPages
        return pages
    
    #Extract the data from the file
    def extractData(pdf_file, page):
    
        pdfFileObj = open(pdf_file, 'rb')
        pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
        pageObj = pdfReader.getPage(page)
        data = pageObj.extractText()
        return data
    
    #Get all word counts
    def getWordCount(data):
    
        data=data.split()
        return len(data)
    
    #Cleaning the data with keras nltk
    def cleanData(text):
    
        data = re.sub('[^a-zA-Z]',' ',text) #keep only all small and capital letters. if there is anything else replace with ' '
        data = data.lower()
        data = data.split()
        ps = PorterStemmer() #loved change to common word love
        data = [ps.stem(word) for word in data if not word in set(stopwords.words('english'))]
        data = ' '.join(data)
        return data
    
    #Calculate the speed of the text in the file
    def calculateSpeed(text):
        speed = of_text(text)
        return speed

    def calTimeOfPdf(pdfFile):

        try:
            if os.path.exists(pdfFile):
                print("\nPDF file found .........................!\n")
        except OSError as err:
            print(err.reason)
            exit(1)
            
        # get the word count in the pdf file
        print("PDF file loading .........................!\n")
        numPages = cal_time.getPageCount(pdfFile)
        text=""
        for i in range(numPages):
            text += cal_time.extractData(pdfFile, i)

        #print (totalWords)
        print("PDF file loaded .........................!\n")
        print("Started : Cleaning data process is started .........................!\n")
        text = cal_time.cleanData(text)
        
        reading_time = cal_time.calculateSpeed(text)
        print("Started : PDF file time calculate .........................!\n")
        
        seconds = reading_time.seconds
        
        minutes, seconds = divmod(seconds, 60)
        
        print("Finished : PDF file time calculated .........................!\n")
        return minutes+1
#def main():
#    if len(sys.argv)!=2:
#        print('command usage: python know_count.py FileName')
#        exit(1)
#    else:
#        pdfFile = sys.argv[1]
#
#        # check if the specified file exists or not
#        try:
#            if os.path.exists(pdfFile):
#                print("\nfile found!\n")
#        except OSError as err:
#            print(err.reason)
#            exit(1)
#
#
#        # get the word count in the pdf file
#        print("Loading pdf...\n")
#        numPages = cal_time.getPageCount(pdfFile)
#        text=""
#        for i in range(numPages):
#            text += cal_time.extractData(pdfFile, i)
#
#        #print (totalWords)
#        print ("PDF loaded\n")
#        text = cal_time.cleanData(text)
#        reading_time = cal_time.calculateSpeed(text)
#        
#        seconds = reading_time.seconds
#        
#        minutes, seconds = divmod(seconds, 60)
#        
#        print(minutes+1)
#
#if __name__ == '__main__':
#    main()