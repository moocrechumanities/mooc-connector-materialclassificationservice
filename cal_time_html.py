import os
import sys
import re
import codecs
import nltk
#import readtime
nltk.download('stopwords')
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
from getReadTime import of_text,of_html,of_markdown


class calTimeHtml():
    #Cleaning the data with keras nltk
    def cleanData(html):
    
        data = re.sub('[^a-zA-Z]',' ',html) #keep only all small and capital letters. if there is anything else replace with ' '
        data = data.lower()
        data = data.split()
        ps = PorterStemmer() #loved change to common word love
        data = [ps.stem(word) for word in data if not word in set(stopwords.words('english'))]
        data = ' '.join(data)
        return data

#Calculate the speed of the text in the file
    def calculateSpeed(html):
        speed = of_html(html)
        return speed

    def calTimeOfHtml(htmlFile):
        try:
            if os.path.exists(htmlFile):
                print("\nfile found .........................!\n")
        except OSError as err:
            print(err.reason)
            exit(1)
    
        print("Loading html content .........................!\n")
        f=codecs.open(htmlFile, 'r')
        html_content = f.read()
                
        print ("html content loaded .........................!\n")
                
        #        calculate the reading time      
        print("Started : html file time calculate .........................!\n")
        
        reading_time = calTimeHtml.calculateSpeed(html_content)
                
        seconds = reading_time.seconds
                
        minutes, seconds = divmod(seconds, 60)
        print("Finished : PDF file time calculated .........................!\n")        
        return minutes+1


