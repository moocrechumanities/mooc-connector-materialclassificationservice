
from utils import read_time,read_time_as_seconds,parse_html


def of_text(text, wpm=None):
    """Get the read time of some text.

    :param text:  String of text (Assumes utf-8).
    """

    return read_time(text, format='text', wpm=wpm)


def of_html(html, wpm=None):
    """Get the read time of some HTML.

    :param html:  String of HTML.
    """

    return read_time(html, format='html', wpm=wpm)


def of_markdown(markdown, wpm=None):
    """Get the read time of some Markdown.

    :param markdown:  String of Markdown.
    """

    return read_time(markdown, format='markdown', wpm=wpm)
